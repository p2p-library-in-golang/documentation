# Projektantrag

| Projektkürzel | Datum      |
| ------------- | ---------- |
| XXXXXXXX      | DD.MM.YYYY |

*XXXXXX ist die Kurzbezeichnung des Projektes. Diese sollte nicht nachträglich geändert werden, da sie als Identifikation an verschiedenen Orten (z.B. git) verwendet wird.*

*Dieser Antrag sollte insgesamt nicht länger als eine A4-Seite sein.*

*Kursiven Text löschen und gegebenenfalls durch gewünschte Angaben ersetzen.*


## Team

*Mitgliederliste des Projektteams mit allen Mailadressen sowie [gitlab.dev.ifs.hsr.ch](https://gitlab.dev.ifs.hsr.ch/) Benutzernamen:*

* Vorname Name <email@hsr.ch> (Gitlab-User: `xyz`)

## Beratungs- und Review-Zeitslots

*Beratungs- und Review-Zeitslots angeben, an denen **alle** Teammitglieder anwesend sein können:*

```
 X  = Slot ist dem Team möglich
(X) = Slot ist für das Team nicht optimal, wäre aber möglich
    = Slot ist nicht möglich
```


|   *Zeitslot*    | Montag | Dienstag | Mittwoch | Donnerstag | Freitag |
| --------------- | ------ | -------- | -------- | ---------- | ------- |
| **08:00-09:00** | X      | X        | X        | X          | X       |
| **09:00-10:00** | X      | X        | X        | X          | X       |
| **10:00-11:00** | X      | X        | X        | X          | X       |
| **11:00-12:00** | X      | X        | X        | X          | X       |
| **12:00-13:00** | X      | X        | X        | X          | X       |
| **13:00-14:00** | X      | X        | X        | X          | X       |
| **14:00-15:00** | X      | X        | X        | X          | X       |
| **15:00-16:00** | X      | X        | X        | X          | X       |
| **16:00-17:00** | X      | X        | X        | X          | X       |
| **17:00-18:00** | X      | X        | X        | X          | X       |
| **18:00-19:00** | X      | X        | X        | X          | X       |

*Hinweis: Sie sollten **mindestens** 5 Slots ausgewählt haben.*

## Motivation

*Was wollen Sie mit dem Projekt erreichen?*

*Für wen ist das zu entwickelnde System gedacht?*

*usw.*

## Programmidee

*Beschreibung der Funktionalität und des Umfangs des zu entwickelndes Systems (ca. 0.5 Seiten).*

## Realisierung

*Auf welcher Plattform*

*Mit welchen Programmiersprache(n), Bibliotheken und Technologien*

*Mit welchem Entwicklungssystem*

*Entwicklerwerkzeuge, die Sie einsetzen wollen (SVN, Jenkins, Redmine…)*

*Welche Art von Benutzerschnittstelle*

*Welche Art der Datenhaltung*

*usw.*


## *Antrag Virtueller Server*

*Wenn ein virtueller Server gewünscht wird, dann können Sie ihn selber bestellen, sobald Ihnen ein Betreuer zugewiesen ist. Wählen Sie den `Ubuntu Linux 14.4 [SE2 Build-Server]`.*

[http://fs-i.hsr.ch/vServerKiosk/](http://fs-i.hsr.ch/vServerKiosk/)

*Achtung: bei der Kiosk-Website kann man nur die kurzen HSR-Email-Adressen angeben, also **nur** [fmehta@hsr.ch](mailto:fmehta@hsr.ch), die langen Adressen wie [farhad.mehta@hsr.ch](mailto:farhad.mehta@hsr.ch) funktionieren auf dieser Seite nicht.*
