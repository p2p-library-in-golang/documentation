# Projektplan
## Einführung

### Zweck
Dieser Projektplan bildet die Grundlage für die Bachelor- und Studienarbeit "P2P Library in Golang".
Im Projektplan wird detailliert auf die Planung, die Organisation und den Projektablauf eingegangen.

### Gültigkeitsbereich
Dieses Dokument gilt in seiner ersten Phase als Grundlage für diese Arbeit und wird während der Projektlaufzeit fortlaufend aktualisiert.
Es gilt jeweils die aktuellste Version des Dokuments.

## Projekt Übersicht

### Aufgabenstellung
Details zu der Aufgabenstellung sind in der vom Projektbetreuer beschriebenen [Aufgabenstellung](https://gitlab.com/p2p-library-in-golang/documentation/-/blob/master/documentation/aufgabestellung/Aufgabenstellung_P2P_v2.pdf) zu entnehmen.

### Zweck und Ziel
In einem ersten Schritt wird mittels Automatic Repeat Request (ARQ) die Reliability für die auf UDP basierende TomP2P Library implementiert.
Hierbei muss darauf geachtet werden, dass Reflection Angriffe verhindert werden.

Danach gilt es den «Bottleneck Bandwidth and Round-Trip Propagation Time» (BBR) Algorithmus zu integrieren, damit mittels Flusskontrolle eine Überlastung während der Übertragung verhindert werden kann.

Anschliessend wird die Security implementiert. Hierbei kann auf das Noise Framework zurückgegriffen werden.
Zusätzlich soll eine High-Level Socket Schnittstelle (analog zu TCP) implementiert werden.

Die einzelnen Implementationen werden jeweils mit entsprechenden Testcases getestet.
Zudem wird jeweils das Read.me erweitert.

Zum Schluss wird ein Benchmarking durchgeführt, indem die implementierte Lösung mit bekannten Protokollen (z.B. TCP, QUIC, etc.) verglichen wird.

### Lieferumfang
Der Lieferumfang umfasst:
- Den Source Code als Zip Datei und den Link auf die Repository.
- Das Read.me vom Source Code Repository mit der Struktur des Protokolls.
- Ergebnisse des Benchmarkings
- Die Projektdokumentation gemäss den [OST-Richtlinien](https://ostch.sharepoint.com/%3Ab%3A/r/teams/TS-StudiengangInformatik/Freigegebene%20Dokumente/Studieninformationen/Studien-%20und%20Bachelorarbeiten/Informationen/Anleitung%20Dokumentation.pdf?csf=1&web=1&e=T3Zjhp) als PDF.


### Technologien
Die Arbeit wird in der Programmiersprache Golang in der Version 1.13.8 geschrieben.
Als Entwicklungsumgebung wird Goland mit der jeweils aktuellsten Version verwendet.
Weitere Technologien sind noch nicht festgelegt und werden laufend ergänzt.
Technologien, die nicht direkt mit der Entwicklung zu tun haben und projektübergreifende Aktivitäten abbilden, sind unter dem Abschnitt [Übersicht der Tools](https://gitlab.com/p2p-library-in-golang/documentation/-/blob/master/documentation/projektplan/projektplan.md#%C3%BCbersicht-der-tools) verfügbar.

### Annahmen und Einschränkungen
Für eine Bachelorarbeit sind 12 ECTS vorgesehen, für eine Studienarbeit sind es 8 ECTS.
Somit fällt pro Person ein Arbeitsaufwand von 360 Stunden (BA), bzw. 240 Stunden (SA) an.
Für die gesamte Arbeit müssen 960 Stunden investiert werden.
Die Bachelorarbeit muss bis zum 18.06.2021 abgegeben werden.

## Projektorganisation

### Organisationsstruktur
Das Team besteht aus:

![Dominik Dietler](images/dominik-dietler.png)
```eval_rst
+----------------------+------------------------------------------------------+-----------------------------+
| Name                 | Funktion                                             | Kontakt                     |
+----------------------+------------------------------------------------------+-----------------------------+
| Dominik Dietler      | Verantwortung für Continuos Integration und Security | dominik.dietler@ost.ch      |
+----------------------+------------------------------------------------------+-----------------------------+
```

![Fabienne König](images/fabienne-koenig.png)
```eval_rst
+----------------------+------------------------------------------------------+-----------------------------+
| Name                 | Funktion                                             | Kontakt                     |
+----------------------+------------------------------------------------------+-----------------------------+
| Fabienne König       | Verantwortung für Projektmanagement und Testing      | fabienne.koenig@ost.ch      |
+----------------------+------------------------------------------------------+-----------------------------+
```

![Sinthujan Lohanathan](images/sinthujan-lohanathan.png)
```eval_rst
+----------------------+------------------------------------------------------+-----------------------------+
| Name                 | Funktion                                             | Kontakt                     |
+----------------------+------------------------------------------------------+-----------------------------+
| Sinthujan Lohanathan | Verantwortung für Administration und Implementation  | sinthujan.lohanathan@ost.ch |
+----------------------+------------------------------------------------------+-----------------------------+
```

### Teambetreuer
Der Teambetreuer ist Thomas Bocek. Er kann über die E-Mail Adresse `thomas.bocek@ost.ch ` erreicht werden.

## Management Abläufe

### Kostenvoranschlag
Für das Projekt stehen jeweils 12 ECTS für Fabienne und Sinthujan für die Bachelorarbeit und 8 ECTS für Dominik für die Studienarbeit zur Verfügung.
Das summiert sich zu insgesamt 32 ECTS, was 32 * 30h = 960 Stunden entspricht.

Es wird versucht die Bachelorarbeit bis zum 9. Juni 2021 fertigzustellen, obwohl die Deadline von der OST auf den 18. Juni 2021 gesetzt wurde.
Somit werden die Projektmitglieder die eine Bachelorarbeit schreiben pro Woche 24 Stunden für die BA investieren.
Dominik Dietler muss seine Studienarbeit bis zum 04.06.2021 beendet haben.
Daher arbeitet er innerhalb von 14 Wochen je 17 Stunden und 10 Minuten an der SA.
Die Hälfte dieser Zeit wird für Zusammenarbeit beansprucht, in der andere Hälfte soll selbständig gearbeitet werden.

Hochgerechnet ergibt das für die BA Arbeit eine Gesamtstundenzahl von 14W x 24h= 336 Stunden.
Die verbleibenden 24h, die zum komplettieren der erforderlichen 360h fehlen, werden in Semesterwoche 15 investiert.
Dies gilt nicht für die SA, die bereits zwei Wochen früher fertiggestellt sein muss.

Die letzte Woche vor der Abgabe der Bachelorarbeit wird als Puffer für Terminüberschreitungen und andere unvorhersehbare Probleme eingeplant.

### Zeitliche Planung

#### Phasen / Iterationen

##### Phase 1: Inception
In dieser Phase werden die für das das Projekt benötigten Grundlagen erarbeitet.

###### Iteration 1: Grundlagen
Diese Iteration dauert eine Woche und wird von 22.02.2021 - 28.02.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:
Grundlagen erarbeiten in ARQ, KCP, BBR, QUIC und Security Algorithmen.

##### Phase 2: Elaboration
In dieser Phase werden die Anforderungen und der Projektplan erstellt.
Zudem soll ein Proof of Concept zu ARQ gemacht werden.

###### Iteration 2: Proof of Concept
Diese Iteration dauert zwei Wochen und wird von 01.03.2021 - 14.03.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:
- Schreiben des Projektplans
- Einrichten der Dokumentenablage und des Source Codes (GitLab) inkl. TimeTracking
- Implementation eines minimale Client/Server mit ARQ als Proof of Concept
- Einrichten der kompletten CI für den Proof of Concept und die Dokumentenablage eingerichtet

###### Iteration 3: Anforderungen
Diese Iteration dauert zwei Wochen und wird von 15.03.2021 - 28.03.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:
- Erstellen des Domainmodells
- Festlegen der Anforderungen
- Schreiben von Unit Tests für Proof of Concept
- Dokumentation der Struktur des Protokolls


##### Phase 3: Construction
In dieser Phase soll die Implementation fertiggestellt werden.

###### Iteration 4: ARQ
Diese Iteration dauert zwei Wochen und wird von 29.03.2021 - 11.04.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:

- Analyse der Architektur von bestehender Code Basis (KCP und 0-RTT)
- Erstellen der Architektur für dieses Projekt (incl. Testing Architektur)
  - Ausfüllen des SoftwareArchitektur.md Templates
- Ausführliche Konstruktion von ARQ incl. Unit Tests
- Vorkehrungen für die Implementation gegen Reflection Angriffe treffen.
- Integration Tests
- Dokumentation (wie implementiert, was sind die Schwierigkeiten, evtl. Theorie zum Thema)
- Nachführen der Dokumentation und des Readme.md



###### Iteration 5: delivery estimation
Diese Iteration dauert zwei Wochen und wird von 12.04.2021 - 25.04.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:

- Ausführliche Recherche und Analyse von BBR
- BBR Implementation incl. Unit Tests
- Integration Tests
- Dokumentation (wie implementiert, was sind die Schwierigkeiten, evtl. Theorie zum Thema)
- Nachführen der Dokumentation und des Readme.md

###### Iteration 6: BBR
Diese Iteration dauert zwei Wochen und wird von 26.04.2021 - 09.05.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:

- Grundlagen zu Security erarbeiten
- Implementation der Security mit dem Noise Framework
- Integration Tests
- Dokumentation (wie implementiert, was sind die Schwierigkeiten, evtl. Theorie zum Thema)
- Nachführen der Dokumentation und des Readme.md
- High Level Schnittstelle ähnlich wie TCP erstellen
- optional: Schlüsselaustausch über DNS
- optional: RPC

###### Iteration 7: delivery estimation evaluation
Diese Iteration dauert zwei Wochen und wird von 10.05.2021 - 23.05.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:

- Evaluation und Auswahl einer Plattform zum Testen des Protokolls
- Auswahl eines geeigneten Benchmarks für das Protokoll
- Durchführen der Tests und aufzeichnen der Werte für das Benchmarking.
- Dokumentation (wie implementiert, was sind die Schwierigkeiten, evtl. Theorie zum Thema)
- Nachführen der Dokumentation und des Readme.md
- Optional: Benchmarking im Internet

##### Phase 4: Transition
In dieser Phase sollen die letzten noch für das Projekt benötigten Dokumente erarbeitet und die Dokumentation fertiggestellt werden, sodass die Abgabe pünktlich erfolgen kann.
###### Iteration 8: Transition
Diese Iteration dauert zwei Wochen und wird von 24.05.2021 - 06.06.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:

- Erstellen des Management Summarys
- Erstellen des Abstracts (offizieller Termin: 09.06.2021)
- Erstellen des A0 Posters (offizieller Termin: 09.06.2021 Abgabe an Betreuer, 15.06.2021 Abgabe an Studiensekretariat)
- Ergänzen und Strukturieren des Technischen Berichts gemäss OST Richtlinien


###### It9: Abgabe
Diese Iteration dauert zwei Wochen und wird von 07.06.2021 - 13.06.2021 durchgeführt.
Die Iteration umfasst folgende Arbeiten:

- Generierung des finalen Dokuments als PDF von md file
- Dokument ausdrucken und binden
- Dokumente auf eprints.hsr.ch hochladen
- Vorbereitung der Präsentation
- Restliche Zeit reserviert für nicht vorgesehene Arbeiten

#### Meilensteine

##### Meilenstein 1: Grundlagen
Das Datum für diesen Meilenstein ist auf den 01.03.2021 festgelegt.
Es sieht folgende work products in md Format vor:
- Dokumentation ARQ
- Dokumentation KCP
- Dokumentation BBR
- Dokumentation QUIC
- Dokumentation Security Algorithmen.

##### Meilenstein 2: Proof of Concept
Das Datum für diesen Meilenstein ist auf den 15.03.2021 festgelegt.
Es sieht folgende work products vor:
- Projektplan 
- Dokumentenablage für Dokumente der Dokumentation
- Dokumentenablage für Source Code
- TimeTracking Einrichtung mit Tmertic
- Proof of Concept ARQ
- CI Einrichtung für Proof of Concept und Dokumentenablage

##### Meilenstein 3: Anforderungen
Das Datum für diesen Meilenstein ist auf den 29.03.2021 festgelegt.
Es sieht folgende work products vor:
- Domain model
- Use cases brief
- Use case diagramm
- Unit Tests zum Proof of Concept (so viel wie möglich)
- Struktur des Protokolls

##### Meilenstein 4: ARQ
Das Datum für diesen Meilenstein ist auf den 12.04.2021 festgelegt.
Es sieht folgende work products vor:
- Geplante Software Architektur Dokumentation
- ARQ Implementation mit Vorkehrungen gegen Reflection Angriffe.
- Integration Tests (so viel wie möglich)
- Dokumentation (Wie implementiert, Was für Schwierigkeiten, Evtl. Theorie zum Thema)
- Nachgeführte Dokumentation Readme.md

##### Meilenstein 5: BBR
Das Datum für diesen Meilenstein ist auf den 26.04.2021 festgelegt.
Es sieht folgende work products vor:
- Dokumentation Recherche zu BBR
- BBR Implementation
- Unit- und Integration Tests (so viel wie möglich)
- Dokumentation (Wie implementiert, Was für Schwierigkeiten)
- Nachgeführte Dokumentation Readme.md


##### Meilenstein 6: Security
Das Datum für diesen Meilenstein ist auf den 10.05.2021 festgelegt.
Es sieht folgende work products in md Format vor:
- Dokumentation Funktionsweise Noise Framework
- Security Implementation
- High Level Schnittelle
- Unit- und Integration Tests (so viel wie möglich)
- Dokumentation (Wie implementiert, Was für Schwierigkeiten)
- Nachgeführte Dokumentation Readme.md

##### Meilenstein 7: Benchmarking
Das Datum für diesen Meilenstein ist auf den 24.05.2021 festgelegt.
Es sieht folgende work products in md Format vor:
- Dokumentation Evaluation und Auswahl einer Testplattform für das Protokoll
- Dokumentation Evaluation und Auswahl eines Benchmarks für das Protokoll
- Durchführen und protokollieren der Performance Tests
- Dokumentation (Wie implementiert, Was für Schwierigkeiten, Evtl. Theorie zum Thema)
- Dokumentation Readme.md nachführen

##### Meilenstein 8: Transition
Das Datum für diesen Meilenstein ist auf den 07.06.2021 festgelegt.
Es sieht folgende work products in md Format vor:
- Management Summary
- Abstract
- A0 Poster
- Technischer Bericht

##### Meilenstein 9: Abgabe
Das Datum für diesen Meilenstein ist auf den 14.06.2021 festgelegt.
Es sieht folgendes work product in md Format vor:
- Sämtliche für die Abgabe vorgesehen Dokumente


### Besprechungen
Besprechungen im Team finden bis zum Ende des Projekts wöchentlich jeweils am Montag, Dienstag und Mittwoch von 13:00 - 17:00 Uhr online via [Microsoft Teams](https://www.microsoft.com/de-ch/microsoft-teams/group-chat-software) statt.
Damit soll durch alle der aktuellen Projektstand kommunizieren, die anstehende Arbeiten besprochen und nicht selbständig lösbare Probleme (z.B. Erstellung vom Projektplan) zusammen bewältigt werden.

Besprechungen mit dem Betreuer finden bis zum Projektende jeden Montag von 13:30 - 14:00 Uhr statt.
Zusammen mit dem Betreuer werden hier offene Fragen diskutiert, der aktuelle Projektstand (IST und SOLL) kommuniziert und allfällige Aufgaben, die das Team oder der Betreuer zu erledigen haben, notiert.
Bei jedem Meilenstein soll dem Betreuer eine kleine Demo gezeigt werden.

## Risikomanagement

### Risiken
Die Risiken sind in einem [seperaten Dokument](https://gitlab.com/p2p-library-in-golang/documentation/-/blob/master/documentation/projektplan/risikoanalyse.md) ausgewiesen.


## Arbeitspakete
Die Arbeitspakete ergeben sich direkt aus der Iteration und den dazugehörigen Work Products, die in den Meilensteinen beschrieben sind.
Sie werden nach den wöchentlichen Sitzungen im Teams erstellt und abgearbeitet.
Als Tool wir das Projektmanagement Tool von GitLab verwendet.
Logins und weitere Details dazu sind im Abschnitt [Projektmanagement](https://gitlab.com/p2p-library-in-golang/documentation/-/blob/master/documentation/projektplan/projektplan.md#projektmanagement) verzeichnet.

## Infrastruktur

### Übersicht der Tools

```eval_rst
+-----------------+---------------------------------------------------+
| Bezeichnung     | Verwendungsgrund                                  |
+-----------------+---------------------------------------------------+
| GitLab          | Versionsverwaltung und CI                         |
+-----------------+---------------------------------------------------+
| Atom            | Verfassung der Dokumentation im markdown Format   |
+-----------------+---------------------------------------------------+
| TMetric         | Zeiterfassung                                     |
+-----------------+---------------------------------------------------+
| Goland          | IDE für Entwicklung mit Golang                    |
+-----------------+---------------------------------------------------+
| Microsoft Teams | Zur Kommunikation mit dem Betreuer und teamintern |
+-----------------+---------------------------------------------------+
```

## Qualitätsmassnahmen

Als Qualitätsmassnahmen wurden für dieses Projekt die folgenden Massnahmen und ihre entsprechenden Ziele aufgestellt:

```eval_rst
+--------------------------------------------------------------------------------------------------+------------------------------------------------------+
| Massnahme                  | Ziel                                                                                             | Zeitraum                                             |
+============================+==================================================================================================+======================================================+
| Read.me aktualisieren      | Jeder soll den Code einfach installieren und ausführen können. High Level Beschreibung des Codes | Alle zwei Wochen                                     |
+----------------------------+--------------------------------------------------------------------------------------------------+------------------------------------------------------+
| Testautomatisierung        | Vermeidung von Programmierfehlern                                                                | Vor/während/nach dem Schreiben von produktivem Code  |
+----------------------------+--------------------------------------------------------------------------------------------------+------------------------------------------------------+
| Reviews der Meilensteine   | Präsentation der Ergebnisse, Überprüfung des Fortschritts                                        | Alle zwei Wochen                                     |
+----------------------------+--------------------------------------------------------------------------------------------------+------------------------------------------------------+
| Treffen im Team            | Vermeidung von Kommunikationsfehlern                                                             | Wöchentlich: Mo, (Di), Mi jeweils von 13:00 - 17:00  |
+----------------------------+--------------------------------------------------------------------------------------------------+------------------------------------------------------+
| Doku Reviews               | Fehler in der Dokumentation (z.B. Rechtschreibefehler, etc.) sollen vermieden werden             | Nach jedem Merge Request                             |
+----------------------------+--------------------------------------------------------------------------------------------------+------------------------------------------------------+
| Continuos Integration (CI) | Sicherstellen, dass nach jeder Änderung der ganze CI Zyklus einheitlich durchläuft    | Nach jeder Änderung                                  |
+----------------------------+--------------------------------------------------------------------------------------------------+------------------------------------------------------+
| Code Reviews               | Vermeidung von Programmierfehlern und Sicherstellung von Code-Qualität                           | Nach jedem Merge Request                             |
+----------------------------+--------------------------------------------------------------------------------------------------+------------------------------------------------------+
```

### Dokumentation
Die Dokumentation befindet sich auf als separates Projekt auf [GitLab](https://gitlab.com/p2p-library-in-golang/documentation).
Die Qualität des Dokuments wird durch Merge-Requests sichergestellt.

### Projektmanagement
Für das Projektmanagement wird [GitLab](gitlab.com) verwendet.
Die jeweils aufgewendete Zeit wird mittels [TMetric](Tmetric.com) erfasst.

Für die jeweiligen Arbeitspakete werden auf GitLab Issues erstellt.
Das TimeTracking wird mit einem Add-on von TMetric für den Client-Browser (z.B. Chrome) realisiert.
Das installierte Add-on blendet bei jedem Issue auf GitLab ein Feld ein, in das die Zeiten eingetragen werden können.
Die Zeiten werden dann auf dem entsprechenden TMetric Projekt gespeichert.
Dadurch kann jederzeit ein Report erstellt werden.

GitLab verfügt zwar selbst über TimeTracking Möglichkeiten, jedoch ist die Bedienung dort relativ umständlich und es können daraus auch keine Time Report Grafiken erzeugt werden.

Die Loginnamen für TMetric sehen wie folgt aus:

```eval_rst
+----------------------+-----------------------------+-----------------------------+
| Name                 | Loginname für GitLab.com    | Loginname für TMetric.com   |
+----------------------+-----------------------------+-----------------------------+
| Dominik Dietler      | domo.d@gmx.ch               | dominik.dietler@gmx.ch      |
+----------------------+-----------------------------+-----------------------------+
| Fabienne König       | fabienne.koenig@outlook.com | fabienne.koenig@outlook.com |
+----------------------+-----------------------------+-----------------------------+
| Sinthujan Lohanathan | sinthujan.lohanathan@ost.ch | sinthujan.lohanathan@ost.ch |
+----------------------+-----------------------------+-----------------------------+
| Thomas Bocek         | tom@tom2p2.net              |                             |
+----------------------+-----------------------------+-----------------------------+
```

### Entwicklung
Der Source Code des Projektes befindet sich auf [GitLab](https://gitlab.com/p2p-library-in-golang/code).

#### Vorgehen
Als erstes wird im Team mit dem Betreuer besprochen, welche Features für den anstehenden Sprint entwickelt bzw. welche Fehler behoben werden sollen.
Dann werden die besprochenen Features/Bugs in Issues verpackt. Für jedes Issues wird ein Feature-Branch erstellt.
Während des Sprints wird der entsprechende Code mit den dazugehörigen Tests entwickelt.
 Anschliessend werden Merge-Requests erstellt, die sich ein zweites Teammitglied anschaut (4-Augen-Prinzip) und nach bestandener Prüfung in den Master Branch merged.

#### Unit Testing
Für das Testing wird das Go package "testing" verwendet.
Die Code Coverage wird von der Golang Programmiersprache selbst zur Verfügung gestellt.
Mittels command `go test --cover` kann die Testabdeckung überprüft werden und ist auch in der CI selbst konfiguriert.

#### Code Reviews
Code Reviews werden mittels Merge-Request für sämtlichen Code sichergestellt.

#### Code Style Guidelines
Es wird der Code Style Guideline von [Effective Go](https://golang.org/doc/effective_go) verwendet.

### Testen
Das Testen wird noch nicht ausführlich beschrieben.
Es soll während der Implementation geplant und erstellt werden.
