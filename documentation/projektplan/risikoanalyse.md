# Risikoanalyse

## Einführung

### Zweck

Dieses Dokument dient der Abschätzung möglicher vorhandener Risiken bei der Umsetzung im Projekt.
Hierbei werden sowohl die Risiken in Bezug auf die Technologie, als auch auf die Risiken im Arbeitsprozess abgehandelt.

### Gültigkeitsbereich

Durch die Verwaltung des Projekts auf GitLab ist nur die Version, die aktuell unter dem entsprechenden Repository als Markdown gespeichert ist gültig. Diese Version wird laufend aktualisiert.

## Risikomanagement

- Projekt: P2P Library in Golang
- Erstellt am: 02. März 2021
- Autor: Dominik Dietler
- Erwarteter Schaden für Elaboration: 15 h pro Sprint
- Erwarteter Schaden für Construction: 17 h pro Sprint

Die Kosten der einzelnen Risiken werden jeweils mit der Eintrittswahrscheinlichkeit pro Sprint multipliziert.

#### Risiko Elaboration

##### Erwartet
```eval_rst
+----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| Nr | Titel            |  Beschreibung                     |  max.        | Eintritts          | Erwarteter  | Vorbeugung                   | Verhalten beim Eintreten |
|    |                  |                                   |  Schaden [h] | wahrscheinlichkeit | Schaden [h] |                              |                          |
|    |                  |                                   |              | pro Sprint         |             |                              |                          |
+====+==================+===================================+==============+====================+=============+==============================+==========================+
| R1 | Unklare          | Verschiedene Interpretationen des | 10           | 80%                | 8           | Grundsätze in Meeting        | Im Plenum Dokumentation  |
|    | Vision           | Endprodukts führen zu             |              |                    |             | klären vor Arbeitsbeginn     | Synchronisieren          |
|    |                  | widersprüchlichen Dokumenten      |              |                    |             |                              |                          |
+----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| R2 | Requirements     | Technologien sind komplexer       | 20           | 10%                | 2           | Klärung im Vorfeld ob die    | Nachbereitungsaufwand    |
|    | falsch           | als angenommen                    |              |                    |             | Aufgaben des Sprints         | nach dem Sprint          |
|    | interpretiert I  |                                   |              |                    |             | überschaubar und von den     |                          |                     
|    |                  |                                   |              |                    |             | zugeteilten Personen         |                          |
|    |                  |                                   |              |                    |             | ausführbar sind              |                          |
+----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| R3 | Requirements     | Abgegebene Dokumente              | 10           | 30%                | 3           | Alte Projekte                | Nachbereitungsaufwand    |
|    | falsch           | entsprechen nicht                 |              |                    |             | als Inspiration              | nach dem Review          |
|    | interpretiert II | den Anforderungen                 |              |                    |             | nutzen / Beten               |                          |
+----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| R4 | Unlösbare        | Meinungsverschiedenheiten         | 16           | 10%                | 1.6         | Klare Entscheidungsprozesse, | Krisensitzung            |
|    | Differenzen      | im Team verhindern                |              |                    |             | saubere Verteilung           |                          |
|    |                  | den Fortschritt                   |              |                    |             | der Kompetenzen              | zur Lösung des Problems  |
+----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
|    |                  |                                   |              | **Summe**          |14 h 36 m    |                              |                          |
+----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
```

##### Tatsächlich




#### Risiko Konstruktion
##### Erwartet
```eval_rst
+-----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| Nr  | Titel            |  Beschreibung                     |  max.        | Eintritts          | Erwarteter  | Vorbeugung                   | Verhalten beim Eintreten |
|     |                  |                                   |  Schaden [h] | wahrscheinlichkeit | Schaden [h] |                              |                          |
|     |                  |                                   |              | pro Sprint         |             |                              |                          |
+=====+==================+===================================+==============+====================+=============+==============================+==========================+
| R5  | Erfahrungsmangel | Fehler im Aufbau oder             | 30           | 25%                | 7.5         | Direkte Rücksprache          | Lösungsorientierte       |
|     |                  | in der Zusammenarbeit             |              |                    |             | mit Projektbetreuer          | Klärung des entstandenen |
|     |                  |                                   |              |                    |             |                              | Aufwands, Verteilung     |
|     |                  |                                   |              |                    |             |                              | der zusätzlichen Arbeit  |
+-----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| R6  | Requirements     | Technologien sind komplexer       | 12           | 25%                | 3           | Klärung im Vorfeld ob        | Nachbereitungsaufwand    |
|     | falsch           | als angenommen                    |              |                    |             | die Aufgaben des Sprints     | nach dem Sprint          |
|     | interpretiert    |                                   |              |                    |             | überschaubar und von den     |                          |
|     |                  |                                   |              |                    |             | zugeteilten Personen         |                          |
|     |                  |                                   |              |                    |             | ausführbar sind              |                          |
+-----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| R7  | Unlösbare        | Meinungsverschiedenheiten im      | 16           | 10%                | 1.6         | Klare Entscheidungsprozesse, | Krisensitzung            |
|     | Differenzen      | Team verhindern den Fortschritt   |              |                    |             | saubere Verteilung           | zur Lösung des Problems  |
|     |                  |                                   |              |                    |             | der Kompetenzen              |                          |
+-----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| R8  | Unerwartete      | Updates von Windows oder Tools    | 16           | 5%                 | 0.8         | Mindestens 24h vor           | Ersatzgerät organisieren |
|     | Systemupdates    | stören den Workflow               |              |                    |             | Präsentation testen          |                          |
|     |                  |                                   |              |                    |             | der Funktionalität           |                          |
+-----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
| R9  | Versions-        | Falscher Umgang                   | 40           | 10%                | 4           | Unterschiedliche Repos       | Manuelles fixen          |
|     | management       | mit Versionsverwaltungstools      |              |                    |             | pro Team, Teamkommunikation  | der aktuellen Versionen  |
|     |                  |                                   |              |                    |             | über Zuständigkeiten und     |                          |
|     |                  |                                   |              |                    |             | Aufteilung der zuständigen   |                          |
|     |                  |                                   |              |                    |             | Files/Arbeitszeiten          |                          |
+-----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
|     |                  |                                   |              | **Summe**          | 17 h        |                              |                          |
+-----+------------------+-----------------------------------+--------------+--------------------+-------------+------------------------------+--------------------------+
```

### Massnahmen

(Merke: Es geht beim ganzen Dokument nur um die Beschreibung der technischen Risiken des Projektes, d.h. nur projektspezifische Risiken. Allgemeine Risiken wie Hardware Ausfall, Ausfall Teammitglied, usw. sollen NICHT beschrieben werden.)
