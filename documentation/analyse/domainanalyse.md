# Domainanalyse

## Einführung
    
### Zweck

*Zweck des Dokumentes*

### Gültigkeitsbereich

*Gültigkeitsbereich des Dokumentes*

### Referenzen

*Liste aller verwendeten und referenzierten Dokumente, Bücher, Links, usw.*

*Referenz auf ein Glossar Dokument, wo alle Abkürzungen und unklaren Begriffe erklärt werden*

*Die Quellen / Referenzen sollten (falls möglich) automatisch erstellt werden*

### Übersicht

*Übersicht über den restlichen Teil dieses Dokumentes geben und dessen Aufbau erläutern*

## Domain Modell

### Strukturdiagramm

*UML Domaindiagramm*

### Wichtige Konzepte

*Auflistung und Beschreibung der wichtigsten Konzepte (Klassen)*

*Die Klassenbeschreibungen können auch als Kern des Glossars dienen, d.h. ein eigentliches Glossar könnte hier als Tabelle noch angehängt werden*

## Systemsequenzdiagramme

*Diagramme der wichtigsten, sowie der komplizierteren Use Cases (pro Use Case ein Kapitel)*

### *Use Case Name*

*Systemsequenzdiagramm zum Use Case*

## Systemoperationen

*Aufzählung der Systemoperationen*

### Vertrag Systemoperation *Name*

*Vertragskonstrukt mit Name, Querverweise, Operationsbeschreibung, Vorbedingungen und Nachbedingungen*
