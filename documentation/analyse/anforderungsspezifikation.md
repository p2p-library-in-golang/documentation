# Anforderungsspezifikationen

## Einführung

### Zweck

*Zweck des Dokumentes*

### Gültigkeitsbereich

*Gültigkeitsbereich des Dokumentes*

### Referenzen

*Liste aller verwendeten und referenzierten Dokumente, Bücher, Links, usw.*

*Referenz auf ein Glossar Dokument, wo alle Abkürzungen und unklaren Begriffe erklärt werden*

*Die Quellen / Referenzen sollten (falls möglich) automatisch erstellt werden*

### Übersicht

*Übersicht über den restlichen Teil dieses Dokumentes geben und dessen Aufbau erläutern*

## Allgemeine Beschreibung

### Produkt Perspektive

*Produkt Perspektive beschreiben*

### Produkt Funktion

*Allgemeine Beschreibung der Funktionen*

### Benutzer Charakteristik

*Zielgruppe des Produktes*

### Einschränkungen

*Wo sind die Grenzen des Produkts*

### Annahmen

*Was ist unklar und wird angenommen bezüglich des Projektes oder des Produktes*

### Abhängigkeiten

*Von welchen Faktoren hängt das Produkt ab*

## Use Cases

*Detailbeschreibung sämtlicher Use Cases. Erfolgt dieser Teil in einem separaten Dokument kann dieser Punkt weggelassen und unter Punkt 2.7. das entsprechende Dokument referenziert werden*

### Use Case Diagramm

*Use Case Diagramm*

### Aktoren & Stakeholder

*Aufzählung und Beschreibung der Aktoren & Stakeholder*

### Beschreibungen (Brief)

*Alle Use Cases in einzelnen Kapiteln beschreiben im Brief Format*

#### *Use Case Name*

*Beschreibung zum Use Case im Brief Format*

### Beschreibungen (Fully Dressed)

*Spezielle und wichtige Use Cases (normalerweise nur einer oder zwei wichtige Use Cases) in einzelnen Kapiteln beschreiben im Fully Dressed Format*

#### *Use Case Name*

*Beschreibung zum Use Case im Fully Dressed Format*

## Weitere Anforderungen

### Qualitätsmerkmale

*Beschreibung der Qualitätsmerkmale der Software (Verweis auf ISO 9126 als Checkliste)*

### Schnittstellen

*Beschreibung der Schnittstellen der Software*

### Randbedingungen

*Auflistung der wichtigsten Randbedingungen mit einer Beschreibung dazu*
