# Schlussbericht

## Einführung

### Zweck

*Zweck des Dokumentes*

### Gültigkeitsbereich

*Gültigkeitsbereich des Dokumentes*

### Referenzen

*Liste aller verwendeten und referenzierten Dokumente, Bücher, Links, usw.*

*Referenz auf ein Glossar Dokument, wo alle Abkürzungen und unklaren Begriffe erklärt werden*

*Die Quellen / Referenzen sollten (falls möglich) automatisch erstellt werden*

## Zielerreichung

*Objektive Zielerreichung beschreiben*

## Allgemeiner Erfahrungsbericht

*Allgemeine Erfahrungen, welche als Team gesammelt wurden (z.B. in Meetings, beim Auftreten von Problemen, usw.)*

## Persönliche Erfahrungen

*Pro Teammitglied ein Kapitel*

### *Name des Teammitgliedes*

*Die Erfahrungen eines Teammitgliedes während der gesamten Projektdauer*
