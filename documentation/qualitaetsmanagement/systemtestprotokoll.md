# Systemtest-Protokoll

## Einführung

### Zweck

*Zweck des Dokumentes*

### Gültigkeitsbereich

*Gültigkeitsbereich des Dokumentes*

### Referenzen

*Liste aller verwendeten und referenzierten Dokumente, Bücher, Links, usw.*

*Referenz auf ein Glossar Dokument, wo alle Abkürzungen und unklaren Begriffe erklärt werden*

*Die Quellen / Referenzen sollten (falls möglich) automatisch erstellt werden*

## Angaben zur Durchführung

*Beschreibung der getesteten Version (Release) und der Testumgebung (alles was es braucht, um den Test reproduzierbar zu machen)*

## Protokoll

*Kopie der Tabelle aus der Systemtestspezifikation für die aktuelle Durchführung*

| UseCase           | implementiert    | Fehler/Unschönheit                                  | Status                      |
| ----------------- | ---------------- | --------------------------------------------------- | --------------------------- |
| *Use Case Name*   | *ja oder nein*   | *Beschreibung zu möglichen Fehlern/Unschönheiten*   | *Beschreibung des Status*   |
| *...*             | *...*            | *...*                                               | *...*                       |

## Verbesserungsmöglichkeiten

*Diese Kapitel besteht nur falls Verbesserungsmöglichkeiten vorhanden*

### Bekannte Einschränkungen

*Bekannte Einschränkungen erwähnen und erläutern*

### Mögliche Detailverbesserungen

*Daraus resultierende Verbesserungen beschreiben*
