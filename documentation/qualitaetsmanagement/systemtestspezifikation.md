# Systemtest-Spezifikation

## Einführung

### Zweck

*Zweck des Dokumentes*

### Gültigkeitsbereich

*Gültigkeitsbereich des Dokumentes*

### Referenzen

*Liste aller verwendeten und referenzierten Dokumente, Bücher, Links, usw.*

*Referenz auf ein Glossar Dokument, wo alle Abkürzungen und unklaren Begriffe erklärt werden*

*Die Quellen / Referenzen sollten (falls möglich) automatisch erstellt werden*

## Voraussetzungen

*Bedingungen die für einen Test vorausgesetzt werden*

## Vorbereitungen

*Was muss vor einem Test gemacht werden (z.B. Spezielle Einstellungen oder Setup)*

## Systemtest

*Beschreibung der einzelnen Tests der Use Cases*

| Use Case        | Beschreibung                                 |
| --------------- | -------------------------------------------- |
| *Use Case Name* | *Beschreibung des Tests für diesen Use Case* |
| *...*           | *...*                                        |
