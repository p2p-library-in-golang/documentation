Project overview |project|
======================================================

:Abbreviation: |project_abbreviation|
:Description: |project_description|
:Authors: |author| (`email to all <author_email_>`_)
:Version: |version|

.. toctree::
  :hidden:
  :titlesonly:
  :maxdepth: 2

  documentation/index.md
  minutes/index.md
  glossary.rst
  meilensteinreviews.rst
