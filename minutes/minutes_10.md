# Sitzung 10

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 26.04.2021 | 13:30 | 10 |

### Sitzungsteilnehmer / Kürzel
- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden


- Stand des Projektes
  - BBR fast fertig 
  - Präsi teilweise fertig, Code-Teil muss noch rein.

- Weiteres Vorgehen   
  - BBR


### Beschlüsse (Diskussion)
BBR erst diese Woche gemäss Projektplan.
Dominik darf an Zwischenpräsentation teilnehmen.



### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|---------------------|-------------|
|    |    |


### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|   |   |    |    |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 19.04.2021 | 13:30 | 30m |
