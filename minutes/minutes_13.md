# Sitzung 13

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 07.06.2021 | 13:30 | 13 |

### Sitzungsteilnehmer / Kürzel
- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Traktanden
  - Security fertig, muss gemerged werden.
  - Dokumentation dran.
    - Fabienne hat ersten Entwurf mit Grundlagen (Theorie) hochgeladen.
    - Sinthu ist BBR Theorie und Implementation delivery estimate dran.

- Weiteres Vorgehen   
 - Dokumentation weiterarbeiten.



### Beschlüsse (Diskussion)
Poster verschieben bis am nächsten Dienstag.



### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|---------------------|-------------|
|    |    |

### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|   |   |    |    |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 14.06.2021 | 12:00 | 30m |
