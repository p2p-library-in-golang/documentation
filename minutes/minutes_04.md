# Sitzung 04

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 15.03.2021 | 13:30 | 04 |

### Sitzungsteilnehmer / Kürzel

- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Protokoll letzte Sitzung
  - ARQ minimale Implementation mit einigen Fehlern, keine Tests.
  - Projektplan sehr weit fortgeschritten, warten auf Aufgabenstellung, um diese mit Projektplan abzugleichen.


- Stand des Projektes
  - GitLab CI PDFs werden gebuildet (Weitere Details: ddi), Tests könnten theoretisch mit CI durchgeführt werden, jedoch noch keine vorhanden.

 
- Weiteres Vorgehen    
  - Projektplan fertigstellen
  - ARQ in GO bugfixen, Tests schreiben und dokumentieren von Code und Struktur des Protokolls in read.me schreiben.

### Beschlüsse (Diskussion)
- Gitlab Runner
  - Eigenen Runner konfigurieren und VM von Gitlab OST einsetzen. Kontakt: Christian Spielmann
  - Alternative 1: Runner auf microInstanzen bei Amazon.
  - Alternative 2: Runner  lokal bei ddi.


- Abgabe SA/BA: ddi muss mit seiner SA bis W14 fertig sein. Danach dürfen keine Anpassungen von ihm am Projekt vorgenommen werden. Es erfolgt eine Abgabe nach BA-Terminen an tbo.

- Bis diesen Dienstag Abend (16.03.2021) letzten Stand von Code Repository pushen und tbo benachrichtigen, damit er es anschauen kann.

### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|-----------------------------|---------------------|
| Anfragen und VM vorbereiten, dass Konfiguration vorgenommen werden kann  | ddi  |
| Termine SA/BA anpassen in Projektplan  | fko  |
| Push letzter Stand und tbo benachrichtigen   | slo  |

### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
| fko | 15/03/2021 14:00 | 15/03/2021 17:00 | privater Grund  |
| fko | 29/03/2021 14:00 | 29/03/2021 17:00 | privater Grund  |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 22.03.2021 | 13:30 | 30m |
