# Sitzung 05

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 29.03.2021 | 13:30 | 06 |

### Sitzungsteilnehmer / Kürzel
- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Protokoll letzte Sitzung
  - ARQ in GO erweitern, Tests schreiben und dokumentieren von Code und Struktur des Protokolls in read.me schreiben.

- Stand des Projektes
  - Testing wird überarbeitet
  - ARQ fast vollständig (Window Sizing fehlt noch).
  - Code noch nicht dokumentiert.

- Weiteres Vorgehen    
  - ARQ in GO erweitern, Tests schreiben und dokumentieren von Code und Struktur des Protokolls in read.me schreiben.

### Beschlüsse (Diskussion)
- Gegenleser für BA: Frank Koch
- Experte für BA: Guilherme Sperb Machado
- Use case, domainmodel, use case diagram werden nicht gemacht, weil Aufgabenstellung bereits Problem und Funktionalität ausführlich beschreibt.
Dafür ausführliche Dokumentation von Testcases mit https://textart.io/sequence für Darstellung der Interaktion zwischen Client und Server für Testfall.
- Bei Funktionen Timestamp für Tests mitgeben, ausser z.B. bei read, write loop.


### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|-----------------------------|---------------------|
|Eintragen von tbo in Tmetric|slo|


### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
| fko | 29/03/2021 14:00 | 29/03/2021 17:00 | privater Grund  |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 29.03.2021 | 13:30 | 30m |
