# Sitzung 14

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 14.06.2021 | 13:30 | 14 |

### Sitzungsteilnehmer / Kürzel
- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Traktanden
  - Dokumentation:
    - Bereit für Feedback auf Teams.

- Weiteres Vorgehen   
 - Dokumentation fertigstellen.



### Beschlüsse (Diskussion)
Keine ausgedruckten



### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|---------------------|-------------|
|    |    |

### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|   |   |    |    |
