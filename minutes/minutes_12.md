# Sitzung 12

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 31.05.2021 | 13:30 | 12 |

### Sitzungsteilnehmer / Kürzel
- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Traktanden
  - Security ein Bug drin. Sonst Test implementiert.
  - Dokumenentation dran (Sinthu konnte nicht viel dran machen).
  - BBR neue Features implementieren müssen
    - Weil kein Delay simuliert werden konnte.
    - Daher neues Repo gemacht mit Cloud code (die immer auf gcloud bis Projektende laufen soll.)
    - Paralleität musste weg (Geht jetzt viel scheneller sequentiell) -> ACK werden ziehmlich genau bei Ankunft registriert. Wichtig für delivery estimation.


- Weiteres Vorgehen   
 - Feature Freeze diese Woche.
 - Dokumentation weiterarbeiten.


### Beschlüsse (Diskussion)
- Dominik letztes Meeting --> BA?

- RFC 9000 QUIC für Fabienne interessant.



### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|---------------------|-------------|
|    |    |

### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|   |   |    |    |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 17.05.2021 | 13:30 | 30m |
