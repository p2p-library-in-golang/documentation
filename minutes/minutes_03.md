# Sitzung 03

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 08.03.2021 | 13:30 | 03 |

### Sitzungsteilnehmer / Kürzel

- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Protokoll letzte Sitzung
  - ARQ minimale Implementation konnte nicht fertig werden (Gründe: Wenig Zeit wegen privaten und beruflichen Arbeiten).
  - Projektplan sehr weit fortgeschritten.


- Stand des Projektes
 - GitLab CI PDFs werden gebuildet (Weitere Details: ddi)
 - Projektplan muss noch fertig gemacht werden: Mergen und Testing, Arbeitspakete, Risiken Teil in Projektplan.
 - ARQ Code von ATP-GO angeschaut und Teile übernommen.


- Weiteres Vorgehen    
  - Projektplan fertigstellen.
  - ARQ in GO implementieren.

### Beschlüsse (Diskussion)
- Was muss wie abgegeben werden am Schluss?
 - Source Code,  Dokumentation (siehe Doku für BA Dokument)
 - Spezifikation Header, Window etc. Repository, Doku

### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|-----------------------------|---------------------|
|  |  |


### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|  |  |  |  |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 15.03.2021 | 13:30 | 1h |
