# Sitzung 07

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 12.04.2021 | 13:30 | 08 |

### Sitzungsteilnehmer / Kürzel
- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Protokoll letzte Sitzung
  - Tests schreiben (Integration und Unit)

- Stand des Projektes
  - BBR eingelesen
  - Testumgebung am aufbauen, ein paar Tests geschrieben, laufen. Müssen noch ein paar mehr Tests geschrieben werden.

- Weiteres Vorgehen   
  - BBR


### Beschlüsse (Diskussion)
BBR erst diese Woche gemäss Projektplan


### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|-----------------------------|---------------------|


### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|   |   |    |    |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 19.04.2021 | 13:30 | 30m |
