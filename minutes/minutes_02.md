# Sitzung 02


| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 01.03.2021 | 13:30 | 02 |

### Sitzungsteilnehmer / Kürzel

- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden

- Protokoll letzte Sitzung
 - KCP, BBR, QUIC, ARQ und Security Algorithmen anschauen.
 - Projektplan, GitLab und CI machen
- Stand des Projektes
 - KCP, BBR, QUIC, ARQ und Security Algorithmen zusammengefasst
 - Projektplan begonnen (QM)
 - GitLab aufgesetzt und CI Basic eingerichtet.
- Weiteres Vorgehen    
    - Projektplan fertigstellen
    - Minimales ARQ in GO implementieren.

### Beschlüsse (Diskussion)

- Teilnahme von ddi an diesem Projekt ist möglich.
- Es wird kein virtueller Server der OST gemietet, weil auch über Cloud machbar (und verschiedene Entfernungen, also z.B. Schweiz - USA, testbar).
- Termin wird auf jeweils 13:30 verschoben.
- GitLab.com account von tbo ist tom@tomp2p.net.

### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|-----------------------------|---------------------|
|  |  |


### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|  |  |  |  |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 08.03.2021 | 13:00 | 1h |
