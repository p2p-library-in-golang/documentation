# \_Template


| Datum  | Zeit | Woche     |
|--------|------|-----------|
| *Datum und* | *Zeit der Sitzung* | *Semesterwoche* |

### Sitzungsteilnehmer / Kürzel

- *Name und Vorname + Kürzel (aus Mailadresse)*
- *...*

### Traktanden

- Protokoll letzte Sitzung
- Stand des Projektes
- *Aufzählung von aktuellen Traktanden oder wichtigen Punkten*
- Weiteres Vorgehen    
    - *Aufzählung von weiterem Vorgehen*

### Beschlüsse (Diskussion)

- *Aufzählung von Beschlüssen, evtl. auch wichtige Diskussionspunkte*

### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|-----------------------------|---------------------|
| *Offene Punkte beschreiben* | *Kürzel der Person* |
| *...*                       | *...*               |

### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
| *Kürzel der Person* | *Datum von* | *Datum bis* | *Grund* |
| *...*               | *...*       | *...*       | *...*   |

## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| *Datum nächste Sitzung* | *Zeit nächste Sitzung* | *Dauer nächster Sitzung* |
