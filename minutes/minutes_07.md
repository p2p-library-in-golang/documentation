# Sitzung 06

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 06.04.2021 | 14:00 | 07 |

### Sitzungsteilnehmer / Kürzel
- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Protokoll letzte Sitzung
  - Tests schreiben

- Stand des Projektes
  - BBR eingelesen
  - Testumgebung am aufbauen, ein paar Tests geschrieben, jedoch laufen sie noch nicht wegen Connector Änderung (multiplex).

- Weiteres Vorgehen    
  - Tests schreiben (Integration und Unit)
  - BBR implementieren

### Beschlüsse (Diskussion)
Testfälle für Packete ohne vorheriges Connectto / Accept ? //evtl. Errorpacket zurücksenden.
Synchronisation von aktueller windowsize wie realisieren? //2^32 machen für Windowsize, 64kb als kleinste Grösse nehmen.
Wie wird Window size realisiert (SEQnr lassen nur 2^16 Nr zu)?
Multiplex funktioniert nicht mit neuen Connectoren ?
sequenzdiGRmmm


### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|-----------------------------|---------------------|


### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|   |   |    |    |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 29.03.2021 | 13:30 | 30m |
