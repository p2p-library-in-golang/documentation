# Sitzung 11

| Datum  | Zeit | Woche     |
|--------|------|-----------|
| 17.05.2021 | 13:30 | 11 |

### Sitzungsteilnehmer / Kürzel
- Thomas Bocek tbo
- Dominik Dietler ddi
- Fabienne König fko
- Sinthujan Lohanathan slo

### Traktanden
- Traktanden
  - BBR funktioniert zwar im Groben, aber im Detail noch nicht.
  - Security Grundgerüst fertig, aber testen ob es wirklich funktioniert.

- Weiteres Vorgehen   
 - Delivery estimation nochmals prüfen und BBR fertigstellen
 - Security testen
 - Dokumentation weitermachen


### Beschlüsse (Diskussion)
- Abstract von Dominik (Er gibt es zusammen mit uns ab?)



### Offene Punkte (erledigt vor nächster Sitzung)

| Was                         | Verantwortlichkeit  |
|---------------------|-------------|
|    |    |


### Kommende Abwesenheiten

| Kürzel              | Von         | Bis         | Grund   |
|---------------------|-------------|-------------|---------|
|   |   |    |    |


## Nächster Termin

| Datum | Zeit | Dauer |
|-------|------|-------|
| 17.05.2021 | 13:30 | 30m |
