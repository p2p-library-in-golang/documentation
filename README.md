# EPJ Documentation Template

[![pipeline status](https://gitlab.com/p2p-library-in-golang/documentation/badges/master/pipeline.svg)](https://gitlab.com/p2p-library-in-golang/documentation/-/commits/master) 

For information on how to use this template,
take a look at the [EPJ Handbook](http://epj.pages.ifs.hsr.ch/handbook/handbook/documentation-template/).

- [Gitlab Pages](https://p2p-library-in-golang.gitlab.io/documentation)
- [Latest master PDF build](https://p2p-library-in-golang.gitlab.io/documentation/_static/docp2p-golang.pdf)
