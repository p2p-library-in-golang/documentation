# Review Meilensteine

Diese Page enthält eine Auflistung der Meilensteine mit den Links zu den zugehörigen Dokumenten, Checklisten und Sitzungsprotokollen.

## MS1: Review Projektplanung

- Dokumente
    - [Projektplan](../documentation/projektplan/projektplan.md)
    - [Risikoanalyse](../documentation/projektplan/risikoanalyse.md)
    - *Zeitauswertung*
- Protokoll
    - *Protokoll DD.MM.YYYY*

## MS2: Review Requirements Analyse

- Dokumente
    - [Anforderungsspezifikation](../documentation/analyse/anforderungsspezifikation.md)
    - [Domainanalyse](../documentation/analyse/domainanalyse.md)
    - *Zeitauswertung*
- Protokoll
    - *Protokoll DD.MM.YYYY*

## MS3: Review End of Elaboration

- Dokumente
    - *Zeitauswertung*
- Protokoll
    - *Protokoll DD.MM.YYYY*

## MS4: Review Architekturdesign

- Dokumente
    - [Softwarearchitektur](../documentation/design/softwarearchitektur.md)
    - *Zeitauswertung*
- Protokoll
    - *Protokoll DD.MM.YYYY*

## MS5: Review Qualitätsmassnahmen

- Dokumente
    - [Qualitätssicherung](../documentation/qualitaetsmanagement/qualitaetssicherung.md)
    - [Systemtestspezifikation](../documentation/qualitaetsmanagement/systemtestspezifikation.md)
    - [Systemtestprotokoll](../documentation/qualitaetsmanagement/systemtestprotokoll.md)
    - *Zeitauswertung*
- Protokoll
    - *Protokoll DD.MM.YYYY*

## Schlusspräsentation

Siehe Moodle

## Schlussabgabe

Siehe Moodle
